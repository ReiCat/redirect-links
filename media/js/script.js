$(document).ready(function() {
    var link_list = $('select#link_list');
    var table_block = $('#table-block');
    if (link_list.val()) {
        var result = json_request('get', '/get_link_detail/'+link_list.val(), '');
        fill_table(table_block, result);
        link_list.change(function() {
            var result = json_request('get', '/get_link_detail/'+$(this).val(), '');
            fill_table(table_block, result);
        });
    }
});

function fill_table(table_block, result) {
    var table_template = _.template('<table>' +
        '<tr><td>Date: </td><td>IP address: </td></tr>' +
        '<% _.each(result, function(result) { %> <tr><td><%= result.date %></td><td><%= result.ip %></td></tr> <% }); %>' +
        '</table>'
    );
    $(table_block).html(table_template({
        result: result
    }));
}

function json_request(method, url, data) {
    var ajaxResponse;
    $.ajax({
        async: false,
        type: method,
        url: url,
        data: data,
        success: function (data) {
            ajaxResponse = data
        },
        dataType: "json"
    });

    return ajaxResponse;
}