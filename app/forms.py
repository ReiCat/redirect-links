from wtforms import Form, StringField, validators, SelectField


class LinkCreationForm(Form):
    url = StringField('URL', [validators.Length(min=4, max=255)])


class LinkListForm(Form):
    link_list = SelectField('Link list')