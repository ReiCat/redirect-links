import datetime

from mongoengine import *

connect('redirectlinks')


class TrackingLink(Document):
    # link_hash = StringField(max_length=40, required=True)
    url = StringField(max_length=255, required=True)


class Click(Document):
    tracking_link = ReferenceField(TrackingLink)
    ip = StringField(max_length=255)
    date_created = DateTimeField(default=datetime.datetime.now)