import json
import random

from hashlib import sha1
from bottle import (
    TEMPLATE_PATH, get, post, request, redirect, static_file, jinja2_template as template
)
from mongoengine import DoesNotExist

from .models import TrackingLink, Click
from .forms import LinkCreationForm, LinkListForm



TEMPLATE_PATH.append("./app/templates")


# def generate_sha1(string, salt=None):
#
#     if not isinstance(string, (str, unicode)):
#         string = str(string)
#     if isinstance(string, unicode):
#         string = string.encode("utf-8")
#     if not salt:
#         salt = sha1(str(random.random())).hexdigest()[:5]
#     generated_hash = sha1(salt+string).hexdigest()
#
#     return generated_hash

@get('/<filename:re:.*\.js>')
def javascripts(filename):
    return static_file(filename, root='media/js')


@get('/')
@post('/')
def index():
    links = TrackingLink.objects.order_by('-id')

    form = LinkCreationForm()
    message = ''
    if request.POST:
        form = LinkCreationForm(request.POST)
        if form.validate():
            url_data = form.url.data
            # generated_hash = generate_sha1(url_data)
            TrackingLink(
                # link_hash=generated_hash,
                url=url_data).save()
            message = 'Link has been successfully saved!'

    return template('form.html', {
        'form': form,
        'links': links,
        'message': message
    })


@get('/link/<link>')
def redirecting_route(link=''):
    try:
        tracking_link = TrackingLink.objects.get(id=link)
        ip = request.remote_addr
        Click(tracking_link=tracking_link, ip=ip).save()
        redirect_link = tracking_link.url
        if not redirect_link.startswith('http://'):
            redirect_link = 'http://' + redirect_link
        return redirect(redirect_link)
    except DoesNotExist:
        return template('error.html', {
            'error': 'Link does not exist!'
        })


@get('/redirect_detail/')
def redirect_detail():
    form = LinkListForm()
    form.link_list.choices = [(link.id, link.id) for link in TrackingLink.objects]
    return template('detail.html', {
        'form': form
    })


@get('/get_link_detail/<link>')
def get_link_detail(link=''):
    try:
        tracking_link = TrackingLink.objects.get(id=link)
        clicks_data = Click.objects(tracking_link=tracking_link)
        dict = {}
        for item in clicks_data:
            dict.update({
                str(item.id): {
                    'date': item.date_created.strftime("%Y.%m.%d %H:%M:%S %Z"),
                    'ip': item.ip
                }
            })
        return json.dumps(dict)
    except DoesNotExist:
        return json.dumps({
            'error': 'Link does not exist!'
        })