import unittest

from webtest import TestApp

from app.models import TrackingLink


def application(environ, start_response):
    response_body = ['%s: %s' % (key, str(value))
                     for key, value in sorted(environ.items())]
    response_body = str('\n'.join(response_body))

    status = '200 OK'
    response_headers = [('Content-Type', 'text/plain'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


class RedirectLinksTestCase(unittest.TestCase):

    def setUp(self):
        self.url = 'http://neti.ee/'
        self.app = TestApp(application)

    def test_index(self):
        response = self.app.get('/')
        self.assertEqual(response.status_int, 200)

    def test_index_post(self):
        response = self.app.post('/', {'url': self.url})
        self.assertEqual(response.status_int, 200)

    def test_redirecting_route(self):
        tracking_obj = TrackingLink(url=self.url).save()
        response = self.app.get('/link/' + str(tracking_obj.id))
        tracking_obj.delete()
        self.assertEqual(response.status_int, 200)

    def test_redirect_detail(self):
        response = self.app.get('/redirect_detail/')
        self.assertEqual(response.status_int, 200)

    def test_get_link_detail(self):
        tracking_obj = TrackingLink(url=self.url).save()
        response = self.app.get('/get_link_detail/' + str(tracking_obj.id))
        self.assertEqual(response.status_int, 200)


suite = unittest.TestLoader().loadTestsFromTestCase(RedirectLinksTestCase)
unittest.TextTestRunner(verbosity=2).run(suite)